# Jack Tester for QAngaroo 

##### Test experiment based on Jack for QAngaroo task

To begin use it, you need to follow these steps
1. Download Jack from https://github.com/uclmr/jack
2. Copy `conf` document in this repo and paste it into the root directionary in Jack
3. Run `donwload.sh` under /data/GloVe/ to download pretrained GloVe
4. Download QAngaroo dataset and unzip it under /data/QAngaroo/
5. Redirect to /data/QAngaroo/, Run `python qangaroo2squad.py qangaroo_v1.1/wikihop/train.masked.json wikihop_train_masked.squad_format.json` and
`python qangaroo2squad.py qangaroo_v1.1/wikihop/dev.masked.json wikihop_dev_masked.squad_format.json` to obtain the 
json data in SQuAD format
6. Run `python ./bin/jack-train.py with config=./conf/qa/Qangaroo/bidaf.yaml` to start the training

Environment Reuqirement
1. Tensorflow
2. sacred==0.7.2
3. spacy=1.9
